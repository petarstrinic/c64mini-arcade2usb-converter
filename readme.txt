Introduction
============
With this converter you can use an old Arcade joystick to a C64 Mini.
It is converting the signal from the old type connector (DB9/Atari-based) to USB.
It can be used on a stock C64 Mini machine, without any modifications.

See change log for history.


Documentation
=============
Full documentation can be found on https://www.tsb.space/projects/c64-mini-arcade2usb-converter/